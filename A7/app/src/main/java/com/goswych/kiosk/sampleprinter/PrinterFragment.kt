package com.goswych.kiosk.sampleprinter

import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.goswych.kiosk.posbankprinter.PrinterApi
import com.goswych.kiosk.posbankprinter.PrinterMsgHandler.Callback
import com.goswych.kiosk.sampleprinter.R.*
import kotlinx.android.synthetic.main.fragment_printer.*
import java.io.IOException

class PrinterFragment : Fragment(), Callback {
    private lateinit var mPrinterHandler: PrinterApi

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView()")
        return inflater.inflate(layout.fragment_printer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated()")
        super.onViewCreated(view, savedInstanceState)

        btn_print_qr.setOnClickListener { onClickPrintQr() }
        btn_toggle_connect.setOnClickListener { onClickToggleConnect() }
        btn_print_receipt.setOnClickListener { onClickPrintReceipt() }
        initPrinter()
    }

    fun initPrinter() {
        Log.d(TAG, "initPrinter()")
        mPrinterHandler = PrinterApi(this, this.context!!)

        if (true) mPrinterHandler.startDiscovery() else discoveryStatus(-1)
    }

    fun onClickPrintQr() {
        Log.d(TAG, "onClickPrintQr()")
        val drawable = resources.getDrawable(drawable.qr, null) as BitmapDrawable
        mPrinterHandler.printBitmap(drawable.bitmap)
    }

    fun onClickToggleConnect() {
        Log.d(TAG, "onClickToggleConnect()")
        mPrinterHandler.toggleConnect()
        btn_toggle_connect.text = if (mPrinterHandler.isConnected) "Disconnect" else "Connect"
    }

    fun onClickPrintReceipt() {
        if (!mPrinterHandler.isConnected) return

        /* Print self test */
        mPrinterHandler.printer?.printSelfTest()

        /* Print receipt sample */
        //InputStream inStream = getResources().openRawResource(R.raw.ks5601_receipt_sample);
        val inStream = resources.openRawResource(raw.receipt_sample_eng)
        var bytesData: ByteArray?
        val nBytes: Int
        try {
            nBytes = inStream.available()
            bytesData = ByteArray(nBytes)
            inStream.read(bytesData)
        } catch (e: IOException) {
            bytesData = null
        }
        if (null != bytesData) {
            mPrinterHandler.printer?.executeDirectIO(bytesData)
        }
    }

    override fun connectionStatus(connected: Boolean, msgID: Int) {
        btn_print_receipt.isEnabled = connected
        btn_print_receipt.setTextColor(if (connected) Color.BLUE else Color.GRAY)
        btn_print_qr.isEnabled = connected
        btn_print_qr.setTextColor(if (connected) Color.BLUE else Color.GRAY)

        if (msgID == 0) return
        val str = (if (connected) "Printer connected" else "Printer disconnected") + " - id: " + msgID
        addLine(str)
    }

    override fun discoveryStatus(status: Int) {
        btn_toggle_connect.isEnabled = status == 1
        btn_toggle_connect.setTextColor(if (status == 1) Color.BLUE else Color.GRAY)

        if (status <= 0) {
            connectionStatus(false,0)
            if (status == 0) addLine("Printer discovering ...")
        }
        else if (status == 1) {
            addLine("Printer discovered")
            addLine(mPrinterHandler.printerDevice.toString())
            addLine("Manufacturer: " + mPrinterHandler.printerDevice?.manufacturer)
            onClickToggleConnect()
        } else {
            addLine("Printer discovery finished\nFound error $status")
        }
    }

    private fun addLine(str: String) {
        tv_output.text = "${tv_output.text?.toString()}\n$str"
    }

    companion object {
        private val TAG = PrinterFragment::class.java.simpleName
    }
}