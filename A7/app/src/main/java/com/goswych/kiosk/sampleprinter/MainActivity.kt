package com.goswych.kiosk.sampleprinter

import android.os.Build
import android.os.Build.VERSION
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Log.d(TAG, "Build.VERSION.CODENAME: " + VERSION.CODENAME + "\n" +
                "Build.VERSION.INCREMENTAL: " + VERSION.INCREMENTAL + "\n" +
                "Build.VERSION.RELEASE: " + VERSION.RELEASE + "\n" +
                "Build.VERSION.SDK_INT: " + VERSION.SDK_INT.toString() + "\n" +
                "Build.BOARD: " + Build.BOARD + "\n" +
                "Build.BOOTLOADER: " + Build.BOOTLOADER + "\n" +
                "Build.BRAND: " + Build.BRAND + "\n" +
                "Build.DEVICE: " + Build.DEVICE + "\n" +
                "Build.DISPLAY: " + Build.DISPLAY + "\n" +
                "Build.FINGERPRINT: " + Build.FINGERPRINT + "\n" +
                "Build.HARDWARE: " + Build.HARDWARE + "\n" +
                "Build.HOST: " + Build.HOST + "\n" +
                "Build.ID: " + Build.ID + "\n" +
                "Build.MANUFACTURER: " + Build.MANUFACTURER + "\n" +
                "Build.MODEL: " + Build.MODEL + "\n" +
                "Build.PRODUCT: " + Build.PRODUCT + "\n" +
                "Build.TAGS: " + Build.TAGS + "\n" +
                "Build.TIME: " + Build.TIME.toString() + "\n" +
                "Build.TYPE: " + Build.TYPE + "\n" +
                "Build.USER: " + Build.USER)
    }

//    override fun onStart() {
//        super.onStart()
//        Log.d(TAG, "onStart()")
//        linkAction(PrinterFragment())
//    }

//    fun linkAction(f: Fragment?) {
//        val ft = supportFragmentManager.beginTransaction()
//        ft.replace(id.contents, f!!)
//        ft.commit()
//    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}