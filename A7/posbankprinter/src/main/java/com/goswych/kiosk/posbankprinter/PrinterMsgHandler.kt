package com.goswych.kiosk.posbankprinter

import android.hardware.usb.UsbDevice
import android.os.Handler
import android.os.Message
import android.util.Log
import com.posbank.printer.PrinterConstants
import com.posbank.printer.PrinterDevice

typealias DiscoveryCallback = () -> Int
class PrinterMsgHandler(var callback: Callback?, val discoveryFinished: DiscoveryCallback) : Handler.Callback {
    interface Callback {
        fun connectionStatus(connected: Boolean, msgID: Int)
        fun discoveryStatus(status: Int)
    }

//    var printerDevice: PrinterDevice? = null
//        private set

    private fun processPrinterMessage_StateChanged(msg: Message) {
        val nState = msg.arg2
        val deviceName = msg.data.getString(PrinterConstants.PRINTER_KEY_STR_DEVICE_NAME)
        when (nState) {
            PrinterConstants.PRINTER_STATE_NONE -> Log.i(TAG, "Printer message received...PRINTER_MSG_STATE_CHANGE : PRINTER_STATE_NONE")
            PrinterConstants.PRINTER_STATE_CONNECTING -> Log.i(TAG, "Printer message received...PRINTER_MSG_STATE_CHANGE : PRINTER_STATE_CONNECTING")
            //showPrinterMessage("Device [" + deviceName + "] connecting...");

            PrinterConstants.PRINTER_STATE_CONNECTED -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_STATE_CHANGE : PRINTER_STATE_CONNECTED")
                callback?.connectionStatus(true, PrinterConstants.PRINTER_STATE_CONNECTED)
            }
        }
    }

    /*private fun processPrinterMessage_DataReceived(msg: Message) {
        val reqCmd = msg.arg1
        val value = msg.arg2
        val obj: Any? = msg.obj
        val data: Bundle = msg.data
        val deviceName = data.getString(PrinterConstants.PRINTER_KEY_STR_DEVICE_NAME)
        var strBufferToast: StringBuffer
        val nComp = 0
        when (reqCmd) {
            PrinterConstants.PRINTER_PROC_GET_STATUS -> {
            }
            PrinterConstants.PRINTER_PROC_GET_PRINTER_ID -> {
            }
            PrinterConstants.PRINTER_PROC_AUTO_STATUS_BACK -> {
            }
            PrinterConstants.PRINTER_PROC_GET_CODE_PAGE -> {
            }
            PrinterConstants.PRINTER_PROC_GET_NV_IMAGE_KEY_CODES -> {
            }
            PrinterConstants.PRINTER_PROC_EXECUTE_DIRECT_IO -> {
            }
        }
    }*/

    override fun handleMessage(msg: Message): Boolean {
        val msgID = msg.what
        /* Not used in this function
            int reqCmd = msg.arg1;
            int value = msg.arg2; */

        val device: PrinterDevice
//        String deviceName = msg.getData().getString(PrinterConstants.PRINTER_KEY_STR_DEVICE_NAME);

        /* Process a received message */
        when (msgID) {
            PrinterConstants.PRINTER_MSG_STATE_CHANGED -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_STATE_CHANGED")
                processPrinterMessage_StateChanged(msg)
            }
            PrinterConstants.PRINTER_MSG_CONN_SUCCEEDED -> Log.i(TAG, "Printer message received...PRINTER_MSG_CONN_SUCCEEDED")
//                if (callback != null) callback.connectionStatus(true, msgID);

            PrinterConstants.PRINTER_MSG_CONN_FAILED, PrinterConstants.PRINTER_MSG_CONN_LOST, PrinterConstants.PRINTER_MSG_CONN_CLOSED -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_CONN_FAILED")
                callback?.connectionStatus(false, msgID)
            }
            PrinterConstants.PRINTER_MSG_DATA_WRITE_COMPLETED -> Log.i(TAG, "Printer message received...PRINTER_MSG_DATA_WRITE_COMPLETED")
            PrinterConstants.PRINTER_MSG_DATA_RECEIVED -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_DATA_RECEIVED")
                //processPrinterMessage_DataReceived(msg)
            }
            PrinterConstants.PRINTER_MSG_DEVICE_NAME -> Log.i(TAG, "Printer message received...PRINTER_MSG_DEVICE_NAME")
            PrinterConstants.PRINTER_MSG_TOAST -> Log.i(TAG, "Printer message received...PRINTER_MSG_TOAST")
            PrinterConstants.PRINTER_MSG_COMPLETE_PROCESS_BITMAP -> Log.i(TAG, "Printer message received...PRINTER_MSG_COMPLETE_PROCESS_BITMAP")
            PrinterConstants.PRINTER_MSG_DISCOVERY_STARTED -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_DISCOVERY_STARTED")
                callback?.discoveryStatus(0)
            }
            PrinterConstants.PRINTER_MSG_DISCOVERY_FINISHED -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_DISCOVERY_FINISHED")
                val status = discoveryFinished()
                callback?.discoveryStatus(status)
            }
            PrinterConstants.PRINTER_MSG_USB_DEVICE_SET -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_USB_DEVICE_SET $msgID")
                device = msg.obj as PrinterDevice
//                if (null != device) {
                val usbDevice = device.deviceContext as UsbDevice
                /* For Debug
                showPrinterMessage(true, "USB Printer [" + device.getDeviceName() + "] found!");
                */
//                if (!Utils.hasPermissionForUSB(usbDevice, context)) {
//                    Utils.requestPermissionForUSB(usbDevice, context)
//                }
//                }
            }
            PrinterConstants.PRINTER_MSG_USB_SERIAL_DEVICE_SET -> {
            }
            PrinterConstants.PRINTER_MSG_SERIAL_DEVICE_SET -> {
                Log.i(TAG, "Printer message received...PRINTER_MSG_SERIAL_DEVICE_SET $msgID")
//                device = msg.obj as PrinterDevice
//                if (null != device) {
                /* For Debug
                showPrinterMessage(true, "SerialPort Printer [" + device.getDeviceName() + "] found!");
                */
//                }
            }
            PrinterConstants.PRINTER_MSG_BLUETOOTH_DEVICE_SET -> Log.i(TAG, "Printer message received...PRINTER_MSG_BLUETOOTH_DEVICE_SET")
            PrinterConstants.PRINTER_MSG_NETWORK_DEVICE_SET -> Log.i(TAG, "Printer message received...PRINTER_MSG_NETWORK_DEVICE_SET")
            PrinterConstants.PRINTER_MSG_ERROR_INVALID_ARGUMENT -> Log.i(TAG, "Printer message received...PRINTER_MSG_ERROR_INVALID_ARGUMENT")
            PrinterConstants.PRINTER_MSG_ERROR_NV_MEMORY_CAPACITY -> Log.i(TAG, "Printer message received...PRINTER_MSG_ERROR_NV_MEMORY_CAPACITY")
            PrinterConstants.PRINTER_MSG_ERROR_OUT_OF_MEMORY -> Log.i(TAG, "Printer message received...PRINTER_MSG_ERROR_OUT_OF_MEMORY")
            PrinterConstants.PRINTER_MSG_ERROR_CLASSES_NOT_FOUND -> Log.i(TAG, "Printer message received...PRINTER_MSG_ERROR_CLASSES_NOT_FOUND")
            PrinterConstants.PRINTER_MSG_ERROR_CMD_NOT_SUPPORTED -> Log.i(TAG, "Printer message received...PRINTER_MSG_ERROR_CMD_NOT_SUPPORTED")
            else -> {
            }
        }
        return true
    }

    companion object {
        private val TAG = PrinterMsgHandler::class.java.simpleName
    }
}