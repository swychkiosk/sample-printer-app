package com.goswych.kiosk.posbankprinter

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import com.posbank.printer.PrinterConstants
import com.posbank.printer.PrinterDevice

object Utils {
    const val PRINTER_USB_PERMISSION = "com.posbank.USB_PERMISSION"
    fun requestPermissionForUSB(device: UsbDevice, context: Context) {
        val permissionIntent: PendingIntent? = PendingIntent.getBroadcast(
                context.applicationContext, 0, Intent(PRINTER_USB_PERMISSION), 0
        )
        val usbManager = context.applicationContext.getSystemService(Context.USB_SERVICE) as UsbManager
        usbManager.requestPermission(device, permissionIntent)
    }

    fun requestPermission(device: PrinterDevice, context: Context) {
        if (device.deviceType == PrinterConstants.PRINTER_TYPE_USB) {
            requestPermissionForUSB(device.deviceContext as UsbDevice, context)
        }
    }

    fun hasPermissionForUSB(device: UsbDevice, context: Context): Boolean {
        val usbManager = context.applicationContext.getSystemService(Context.USB_SERVICE) as UsbManager
        return usbManager.hasPermission(device)
    }

    fun hasPermission(device: PrinterDevice, context: Context): Boolean {
        var hasPermission = true
        if (device.deviceType == PrinterConstants.PRINTER_TYPE_USB) {
            hasPermission = hasPermissionForUSB(device.deviceContext as UsbDevice, context)
        }
        return hasPermission
    }
}