package com.goswych.kiosk.posbankprinter

import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.util.Log
import com.goswych.kiosk.posbankprinter.Utils.hasPermission
import com.goswych.kiosk.posbankprinter.Utils.requestPermission
import com.posbank.printer.Printer
import com.posbank.printer.PrinterConstants
import com.posbank.printer.PrinterDevice
import com.posbank.printer.PrinterManager

class PrinterApi(handlerCb: PrinterMsgHandler.Callback, val context: Context) {

    private val mPrinterManager: PrinterManager
    private val printerMsgHandler: PrinterMsgHandler
    //    private var mPrinterDeviceMap: HashMap<String, PrinterDevice>? = null
    var printer: Printer? = null
        private set
    var printerDevice: PrinterDevice? = null
        private set
//    val printerDevice: PrinterDevice?
//        get() = printerMsgHandler?.printerDevice

    fun startDiscovery() {
        /* Clear all data for reloading */
//        if (null != mPrinterDeviceMap && mPrinterDeviceMap!!.size > 0) {
//            mPrinterDeviceMap!!.clear()
//        }
        mPrinterManager.startDiscovery(PrinterConstants.PRINTER_TYPE_USB)
    }

    internal fun discoveryFinished(): Int {
//        if (null != mPrinterDeviceMap) {
//            mPrinterDeviceMap!!.clear()
//        }
        val mPrinterDeviceMap = mPrinterManager.deviceList
        if (mPrinterDeviceMap?.size == 0) return 2

        //  ...tvo: get 1st printer
        val key = mPrinterDeviceMap?.keys?.iterator()?.next() ?: return 3
        printerDevice = mPrinterDeviceMap[key]
        if (printerDevice!!.deviceType != PrinterConstants.PRINTER_TYPE_USB) return 4
        if (!hasPermission(printerDevice!!, context)) {
            Log.w(TAG, "Needs permission to access " + printerDevice!!.deviceName)
            requestPermission(printerDevice!!, context)
            return 5
        }
        return 1
    }

    val isConnected: Boolean
        get() = printer != null

    fun connect(): Boolean {
        if (isConnected) return true

        /* Connect with a PrinterDevice */
        printer = mPrinterManager.connectDevice(printerDevice)
        return true
    }

    fun disconnect() {
        if (!isConnected) return
        printer?.disconnect()
        printer = null
    }

    fun toggleConnect() {
        val toConnect = !isConnected
        if (toConnect) connect() else disconnect()
    }


    fun printBitmap(bitmap: Bitmap?) {
//        int attribute = PrinterConstants.PRINTER_TEXT_ATTRIBUTE_FONT_A; // 0
//        int size = 0;

        val result = printer!!.printText("Swych gift card", PrinterConstants.PRINTER_ALIGNMENT_CENTER, 0, 0)
        if (PrinterConstants.PRINTER_RESULT_OK != result) {
            return
        }
        printer!!.lineFeed(1)
        printer!!.printBitmap(bitmap, PrinterConstants.PRINTER_ALIGNMENT_CENTER, PrinterConstants.PRINTER_BITMAP_WIDTH_NONE, 50, false)
        printer!!.lineFeed(5)
        printer!!.cutPaper(1)
    }

    init {
        val discoveryLambda = { discoveryFinished() }
        printerMsgHandler = PrinterMsgHandler(handlerCb, discoveryLambda)
        val printerHandler = Handler(printerMsgHandler)
        /* New instance and start discovery */
        mPrinterManager = PrinterManager(
                context,         /* Application Context */
                printerHandler,  /* Message handler */
                null      /* looper */
        )
    }

    companion object {
        private val TAG = PrinterMsgHandler::class.java.simpleName
    }
}